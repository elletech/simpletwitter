package chapter6.beans;

import java.io.Serializable;
import java.util.Date;

public class Message implements Serializable {
    private static final long serialVersionUID = 1L;

    private int id;
    private int userId;
    private String text;
    private Date createdDate;
    private Date updatedDate;

	public void setText(String text) {
		this.text = text;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public int getUserId() {
		return userId;
	}

	public String getText() {
		return text;
	}

}