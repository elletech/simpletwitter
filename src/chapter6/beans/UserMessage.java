package chapter6.beans;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

public class UserMessage implements Serializable {
    private static final long serialVersionUID = 1L;

    private int id;
    private String account;
    private String name;
    private int userId;
    private String text;
    private Date created_date;

	public void setName(String name) {
		this.name = name;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public void setText(String text) {
		this.text = text;
	}

	public void setCreated_date(Timestamp createdDate) {
		this.created_date = createdDate;
	}

	public String getAccount() {
		return account;
	}

	public String getName() {
		return name;
	}

	public String getText() {
		return text;
	}

	public int getUserId() {
		return userId;
	}

	public Date getCreated_date() {
		return created_date;
	}
}